import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View, Image } from 'react-native';

const MovieListItem = (props) => {
  const { item } = props;
  return (
    <View style={styles.container}>
      <View style={styles.row}>
        <Image style={styles.image} source={{ uri: `http://image.tmdb.org/t/p/w185/${item.poster_path}` }}></Image>
        <View style={styles.column}>
          <Text style={styles.title}>{item.title}</Text>
          <Text style={styles.genre}>{item.genre_ids.join(', ')}</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 15,
    flex: 1
  },
  row: {
    flexDirection: 'row',
    height: 100
  },
  image: {
    height: 200,
    flex: 1,
    borderRadius: 2
  },
  title: {
    padding: 5,
    fontSize: 18,
    flex: 1,
    fontWeight: 'bold'
  },
  genre: {
    padding: 5,
    fontSize: 10,
    flex: 2,
  },
  column: {
    flexDirection: 'column',
    flex: 3
  }
});

MovieListItem.propTypes = {
  item: PropTypes.object.isRequired
};

export default MovieListItem;
