import React from 'react';
import PropTypes from 'prop-types';
import { FlatList } from 'react-native';
import MovieListItem from './MovieListitem';

const MovieList = (props) => {
    return (
        <FlatList
            data={props.movieList}
            renderItem={({ item }) => <MovieListItem item={item}></MovieListItem>}
            keyExtractor={(item, index) => index.toString()}
        />
    );
}

MovieList.propTypes = {
    movieList: PropTypes.array.isRequired
};

export default MovieList;