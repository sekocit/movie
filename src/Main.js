import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Modal, Picker, Button, Slider, ActivityIndicator } from 'react-native';
import MovieList from './components/MovieList';

export default class Main extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: true, // Activity Indicator
      modalVisible: false, // Filter Modal
      ratingValue: 3, // Rating Default Value
      ratingStep: 0.5, // Rating Slider Step
      ratingMaxValue: 10, // Rating Slider Max Value
      genreList: [], // Movie Genre List
      movieFullList: [], // Movie Full List
      movieFilteredList: [], // Movie Filtered List (Data binded to the list)
      movieGenre: '', // Movie Genre Selector
    };
  }

  // Calling API after component is mounted
  componentDidMount() {
    fetch('https://api.themoviedb.org/3/movie/popular?page=1&language=en-US&api_key=82e7c883a5278055b726c037cc6c0cf7')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          movieFullList: responseJson.results,
          movieFilteredList: responseJson.results.filter(movie => movie.vote_average >= this.state.ratingValue),
        }, function () {
          fetch('https://api.themoviedb.org/3/genre/movie/list?api_key=82e7c883a5278055b726c037cc6c0cf7')
            .then((response) => response.json())
            .then((responseJson) => {
              this.setState({
                genreList: responseJson.genres,
              }, function () {
                let list = this.state.movieFilteredList;
                let genres = responseJson.genres;
                list = list.map(item => {
                  return item.genre_ids = item.genre_ids.map(i => {
                    let item2 = genres.find(i2 => i2.id === i);
                    return item2.name;
                  })
                })
                this.setState({
                  isLoading: false,
                })
              });
            })
            .catch((error) => {
              console.error(error);
            });
        });
      })
      .catch((error) => {
        console.error(error);
      });
  }
  // Settings Filter Modal Visibility
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  //Changing movie list according to the filters after close
  onFilterModalClose() {
    let movies = this.state.movieFullList;
    let genres = this.state.genreList;
    let genre;
    if (this.state.movieGenre !== 0) {
      genre = genres.find(genre => genre.id === this.state.movieGenre)
      genre = genre.name;
      this.setState({
        movieFilteredList: movies.filter(movie => movie.vote_average >= this.state.ratingValue && movie.genre_ids.includes(genre))
      })
    }
    else {
      this.setState({
        movieFilteredList: movies.filter(movie => movie.vote_average >= this.state.ratingValue)
      })
    }

  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>Movies</Text>
          <TouchableOpacity onPress={() => {
            this.setModalVisible(true);
          }}>
            <Text style={styles.filterText}>Filters</Text>
          </TouchableOpacity>
        </View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => { }}>
          <View style={styles.filterModal}>
            <Text>Select Genre</Text>
            <Picker
              selectedValue={this.state.movieGenre}
              style={styles.genrePicker}
              onValueChange={(itemValue) => this.setState({ movieGenre: itemValue })}>
              <Picker.Item label="Select" value={0} />
              {this.state.genreList.map(item => <Picker.Item key={item.id} label={item.name} value={item.id} />)}
            </Picker>
            <Text>Select Rating</Text>
            <Text>{this.state.ratingValue}</Text>
            <Slider
              style={styles.ratingSlider}
              minimumTrackTintColor="#0000ff"
              maximumValue={this.state.ratingMaxValue}
              thumbTintColor="#0000ff"
              step={this.state.ratingStep}
              value={this.state.ratingValue}
              onValueChange={value => this.setState({ ratingValue: value })}
            />
            <Button
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
                this.onFilterModalClose();
              }}
              title="Apply Filters"
              color="#0000ff"
            />
          </View>
        </Modal>
        <MovieList movieList={this.state.movieFilteredList}>
        </MovieList>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  header: {
    height: 65,
    backgroundColor: '#0000ff',
    elevation: 3,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  headerText: {
    fontSize: 20,
    color: 'white',
    paddingBottom: 5,
    paddingTop: 20,
    paddingLeft: 10,
  },
  filterText: {
    fontSize: 15,
    color: 'white',
    paddingBottom: 5,
    paddingTop: 20,
    paddingRight: 10,
  },
  filterModal: {
    padding: 10,
    flex: 1,
    marginTop: 20,
  },
  genrePicker: {
    width: 200,
    height: 50,
    marginBottom: 20,

  },
  ratingSlider: {
    marginTop: 20,
    marginBottom: 20
  }
});
