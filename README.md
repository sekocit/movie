This React Native app is created with create-react-native-app CLI. It is only for demo purpose.

# For Quick View
  - https://expo.io/@sekocit/movie-challenge
  - Download Expo App and scan the QR code


# Installation

  - npm install -g create-react-native-app
  - Clone this repo
  - Download expo app (Android & iOS)
  - Run 'expo start' and scan QR code on app
  
# Features

  - All UI is created from scratch with react native components. No additional library is used.
  - No other dependencies(packages) are used.